window.App = Ember.Application.create();

App.ApplicationAdapter = DS.FixtureAdapter.extend();


App.Router.map(function () {
  this.resource('index', {path: "/" });
  this.resource('books', {path: "/books" });
  this.resource('book', function(){
    this.route("show", { path: '/:book_id'});
  });
  
  this.resource('authors', {path: "/authors" });
  this.resource('genre', function(){
    this.route("show", { path: '/:genre_id'});
  });
  
  this.resource('author', function(){
    this.route("show", { path: '/:author_id'});
  });
});


App.Book = DS.Model.extend({
  Name: DS.attr('string'),
  Genre: DS.attr('array'),
  Description: DS.attr('string'),
  authors: DS.attr('array')
});

App.BooksRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.set('model', this.get('store').find('book'));
  }
});


App.BooksController = Ember.ArrayController.extend({
  sortProperties: ['created_at'],
  sortAscending: false 
});


App.Book.FIXTURES = [
  {
     "id": 1,
     "Name":"four years at 2ch",
     "Genre":{
        "name":"some new Genre",
        "id":1
       },
     "Description":"some cool book",
     "authors":[
              {
              "id":1,
              "name":"some Dude"
              }
             ]
    }, 
  {
       "id": 2,
       "Name":"rap is my bread",
       "Genre":{
        "name":"some new Genre",
        "id":2
       },
       "Description":"another cool book",
       "authors":[
                {
                "id":2,
                "name":"some homie"
                }
               ]
      },
      {
       "id": 3,
       "Name":"Gordon Freeman saved my life",
       "Genre":{
        "name":"some new Genre",
        "id":3
       },
       "Description":"some some some",
       "authors":[
                {
                "id":2,
                "name":"some homie"
                }
               ]
      }
];
App.Author = DS.Model.extend({
  Name: DS.attr('string'),
  books: DS.attr('array'),
  bio: DS.attr('string'),
});


App.AuthorsRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.set('model', this.get('store').find('author'));
  }
});

App.AuthorsController = Ember.ArrayController.extend({
  sortProperties: ['created_at'],
  sortAscending: false 
});
App.Author.FIXTURES = [
  {
     "id":"1",
     "Name":"someDude",
     "books":[
              {
                "id":1,
                "name":"four years at 2ch"
              }
             ],
     "bio":"someDude was born in 1970"
    }, 
 {
     "id":"2",
     "Name":"some homie",
     "books":[
              {
                "id":2,
                "name":"rap is my bread"
              },
              {
                "id":3,
                "name":"Gordon Freeman saved my life"
              }

             ],
     "bio":"some homie was a cool dude"
    }
];

App.Genre = DS.Model.extend({
  Name: DS.attr('string'),
  books: DS.attr('array'),
});


App.GenresRoute = Ember.Route.extend({
  setupController: function(controller) {
    controller.set('model', this.get('store').find('genre'));
  }
});


App.GenresController = Ember.ArrayController.extend({
  sortProperties: ['created_at'],
  sortAscending: false
});

App.Genre.FIXTURES = [
  {
     "id":1,
     "Name":"some Genre",
     "books":[
              {
                "id":1,
                "name":"four years at 2ch"
              }
             ]
    },
    {
     "id":2,
     "Name":"some new Genre",
     "books":[
              {
                "id":2,
                "name":"rap is my bread"
              }
             ]
    },
    {
     "id":3,
     "Name":"cool Genre",
     "books":[
              {
                "id":3,
                "name": "Gordon Freeman saved my life"
              }
             ]
    }
];

App.ItemView = Ember.View.extend({
    tagName: 'li',
    click: function() {
        $('#dLabel').dropdown();
        console.log("sup");
        // $('li').next('.dropdown').addClass('open');
    }
});